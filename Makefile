install: build start vendors perms migrate

build:
	docker-compose build
bash:
	docker-compose exec php bash
perms:
	docker-compose exec php chown www-data -R /var/www/symfony/var /var/www/symfony/public
migrate:
	docker-compose exec php symfony console doctrine:migrations:migrate
start:
	docker-compose up -d
stop:
	docker-compose stop
vendors:
	docker-compose exec php composer install

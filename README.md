# Rock band

## Getting started

```
git clone https://gitlab.com/mozzy/rock-band.git
```

## Description
We want to provide an excel import feature of rock bands for our customer.
This project handles the backend part (import + CRUD).

### Technical stack

- REST API (API Platform with Symfony 6)
- DB (PostgreSQL)
- Documentation (Swagger API)
- PhpSpreadsheet to parse excel file

## Installation

### Prerequisites
- Docker compose

Clone the project then run the following commands to build your container and then start them.
```
make install
```



Whenever you want to start containers :
```
make start
```

## Usage
- Swagger documentation is available at http://localhost:8081/api
You can test CRUD routes there.
- An import form is available here : http://localhost:8081/rockband/importform
- Import route can also be tested using postman by posting file param to this url: http://localhost:8081/rockband/import
- Adminer database web interface is available here: http://localhost:8080

## Roadmap
- Batch insert entities, keep single transation

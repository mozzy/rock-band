<?php

namespace App\Controller;

use App\Service\RockBandImport;
use App\Service\RockBandImportFileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;

class RockBandController extends AbstractController
{
    #[Route('/rock/band', name: 'app_rock_band')]
    public function index(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/RockBandController.php',
        ]);
    }

    #[Route('/rockband/import/success', name: 'app_rock_band_import_success')]
    public function success(): Response
    {
        return $this->render('rockband/importsuccess.html.twig');
    }

    #[Route('/rockband/importform', name: 'app_rock_band_import_form')]
    public function importform(Request $request, RockBandImportFileUploader $rockBandImportFileUploader, RockBandImport $rockBandImport): Response
    {
        $form = $this->createFormBuilder()
            ->add('file', FileType::class, [
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid excel document',
                    ])
                ],
            ])
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $file */
            $file = $form->get('file')->getData();
            if ($file) {
                $filepath = $rockBandImportFileUploader->upload($file);
                $success = $rockBandImport->process($filepath);

                if ($success) {
                    return $this->redirectToRoute('app_rock_band_import_success');
                }
            }
        }

        return $this->renderForm('rockband/importform.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/rockband/import', name: 'app_rock_band_import')]
    public function import(Request $request, RockBandImportFileUploader $rockBandImportFileUploader, RockBandImport $rockBandImport): JsonResponse
    {
        /** @var UploadedFile $file */
        $file = $request->files->get('file');

        if ($file->isValid()) {
            $filepath = $rockBandImportFileUploader->upload($file);
            $success = $rockBandImport->process($filepath);

            return $this->json([
                'success' => $success,
                'message' => $success ? 'Import successful' : 'Something went wrong'
            ]);
        }

        return $this->json([
            'success' => false,
            'message' => 'Something went wrong'
        ]);
    }
}

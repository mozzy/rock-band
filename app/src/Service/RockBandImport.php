<?php

namespace App\Service;

use App\Entity\RockBand;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader;
use Psr\Log\LoggerInterface;

class RockBandImport
{
    private EntityManagerInterface $entityManager;
    private LoggerInterface $logger;

    public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    public function process($filepath): bool
    {
        try {
            $this->logger->info('START RockBand import from file: ' . $filepath);
            // This library deals with sanitizing (whitespaces, non-breakable spaces, line return..).
            $spreadsheet = IOFactory::load($filepath);
            $worksheet = $spreadsheet->getActiveSheet();
            // Skip header.
            $worksheet->removeRow(1);
            // TODO: handle batch with multiple insert.

            foreach ($worksheet->getRowIterator() as $row)
            {
                $rockBand = new RockBand();

                foreach ($row->getCellIterator() as $colIndex => $cell) {
                    switch ($colIndex) {
                        case 'A':
                            $rockBand->setName($cell->getValue());
                            break;
                        case 'B':
                            $rockBand->setCountry($cell->getValue());
                            break;
                        case 'C':
                            $rockBand->setCity(ucwords($cell->getValue()));
                            break;
                        case 'D':
                            $rockBand->setStartYear($cell->getValue());
                            break;
                        case 'E':
                            $rockBand->setEndYear($cell->getValue());
                            break;
                        case 'F':
                            $rockBand->setFounders($cell->getValue());
                            break;
                        case 'G':
                            $rockBand->setMembers($cell->getValue());
                            break;
                        case 'H':
                            $rockBand->setGenre($cell->getValue());
                            break;
                        case 'I':
                            $rockBand->setBio($cell->getValue());
                            break;
                    }
                }

                $this->entityManager->persist($rockBand);
            }
            $this->logger->info('Flushing entities');
            $this->entityManager->flush();
            $this->logger->info('END RockBand import');

            return true;
        } catch (\PhpOffice\PhpSpreadsheet\Exception|Reader\Exception $e) {
            $this->logger->error($e->getMessage());
            return false;
        }
    }
}

<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

class RockBandImportFileUploader
{
    private string $targetDirectory;
    private SluggerInterface $slugger;

    public function __construct(string $targetDirectory, SluggerInterface $slugger)
    {
        $this->targetDirectory = $targetDirectory;
        $this->slugger = $slugger;
    }

    public function upload(UploadedFile $file): string
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $filename = $safeFilename . '-' . md5(uniqid()) . '.xslx';//. $file->guessExtension();
        $filepath = $this->getTargetDirectory() . '/' . $filename;

        try {
            $file->move($this->getTargetDirectory(), $filename);
        } catch (FileException $e) {
            dd($e);
            // TODO: log error $e.
            // TODO forward error to controller
        }

        return $filepath;
    }

    public function getTargetDirectory(): string
    {
        return $this->targetDirectory;
    }
}
